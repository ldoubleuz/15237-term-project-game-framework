Connor Brem (cbrem)
Erik Pintar (epintar)
Leon Zhang (lwzhang)

A game creation framework to create minigames which are populated with
educational questions intended to teach English.

(Created in the course 15-237: Cross Platform Mobile Web Apps at
 Carnegie Mellon University)

Operation notes:
- Currently not hosted anywhere, so to run the application,
  run "node app" from the top level termproject directory.
- View the application by visiting localhost:8000 while a node server is up

Collaboration Notes:
- We worked with the 15-239 group that developed a nice interface for teachers to input educational multiple choice questions to a database
Their site is at:
  http://www.cs.cmu.edu/~239/projects/techcafe-games/

       *** THE QUESTION DATABASE IS FREQUENTLY DOWN ***
 *** if node app seems to hang and then crash this is why ***
       (it usually comes back in about 10 minutes or so)